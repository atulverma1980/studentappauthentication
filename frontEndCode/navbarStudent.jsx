import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
class Navbar extends Component{
    state={
        role:"",
    }
    async componentDidMount(){
        const user =  localStorage.getItem("user");
        if(user){
            let response = await axios.get("http://localhost:2410/student/detail",{
             headers:{Authorization:user}
            });
            let {data}=response;
            this.setState({role:data.map(v=>v.role)});
        }
    }
    render(){
        const user =  localStorage.getItem("user");
       const{role}=this.state;
       console.log(role,"navbar");
        return(
            <nav className="navbar navbar-expand-sm navbar-success  bg-success">
                <div className="container-fluid">
                <Link to="/" className="navbar-brand text-dark">
                    Students
                </Link>
                <div className="">
                    <ul className="navbar-nav mr-auto">
                        {user && role[0]==="Admin" &&(<React.Fragment>
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/student">
                                Show Student
                            </Link>
                        </li>
                       
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/addstudent">
                                Add Student
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/reset">
                                Reset 
                            </Link>
                        </li>
                        </React.Fragment>)}
                        {!user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/login">
                                Login 
                            </Link>
                        </li>)}
                        {user && (
                        <li className="nav-item">
                            <Link className="nav-link text-dark" to="/logout">
                                Logout 
                            </Link>
                        </li>)}
                        
                    </ul>
                </div>
                </div>
            </nav>
        )
    }
} 
export default Navbar;