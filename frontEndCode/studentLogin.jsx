import React from "react";
import http from "./../../services/httpService";
import axios from "axios";
export default class Login extends React.Component{
    state={
        detail:{email:"",password:""},
    }
    async postdata(obj){
            // let response = await http.post("/login",obj);
            const response= await axios.post("http://localhost:2410/login",obj);
            let{headers}=response;
            let token = headers["x-auth-token"];
            localStorage.setItem("user",`bearer ${token}`);
            let resp = await axios.get("http://localhost:2410/student/detail",{
                headers:{Authorization:`bearer ${token}`}
               });
               let {data}=resp;
              
               if(data.find(v=>v.role==="Admin")){
                localStorage.setItem("role","Admin");
                window.location="/student";
               }
               if(data.find(v=>v.role==="Student")){
                   localStorage.setItem("role","Student");
                window.location="/detail";
               }
           
    }
    handleChange=(e)=>{
        let s1={...this.state};
        const{currentTarget:input}=e;
        s1.detail[input.name]=input.value;
        this.setState(s1);
    }
    handleLogin=(e)=>{
        e.preventDefault();
        this.postdata(this.state.detail);
    }
  
    render(){
        const{errors=null}=this.state;
        let {email,password}=this.state.detail;
        return <div className="container">
            <div className="row my-4">
                <div className="col-4"></div>
                <div className="col-4 border bg-light">
                    <div className="form-group">
                        {errors?<span className="text-danger">{errors}</span>:""}
                        <label>Email</label>
                        <input type="text" name="email" value={email} className="form-control" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" name="password" value={password} className="form-control" onChange={this.handleChange}/>
                    </div>
                    <button className="btn btn-secondary btn-sm"onClick={this.handleLogin}>Login</button>
                </div>
              
            </div>
        </div>

    }
}