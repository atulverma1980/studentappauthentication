import React from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import http from "./../../services/httpService";
export default class EditStudent extends React.Component{
    state={
        detail:{id:"",name:"",section:"",maths:"",english:"",computers:"",email:"",gender:"",password:"",role:""},
        
    }
    async componentDidMount(){
        
        try {
            let user = localStorage.getItem("user");
            let response = await axios.get("http://localhost:2410/student/detail",{
                headers:{Authorization:user}
            });
            let s1={...this.state};
            let {data} = response;
            let arr = data.map(v=>{
                s1.detail.id=v.id;
                s1.detail.name=v.name;
                s1.detail.section=v.section;
                s1.detail.maths=v.maths;
                s1.detail.english=v.english;
                s1.detail.computers=  v.computers;
                s1.detail.email=v.email;
                s1.detail.password=v.password;
                s1.detail.gender=v.gender;
                s1.detail.role=v.role;
            });
            this.setState(s1);
        } catch (ex) {
            if(ex.response && ex.response.status==401){
                localStorage.removeItem("role") 
                localStorage.removeItem("user")
                window.location="/login"
            }
        }
    }
    async putdata(url,obj){
        try{
        let response = await axios.put(url,obj);
        this.props.history.push("/detail");
        }catch(ex){
            if(ex.response && ex.response.status==401){
                window.location="/login" 
            }
        }
    }
    handleChange=e=>{
        const{currentTarget:input}=e
        let s1={...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
       }
       handleSubmit=e=>{
        e.preventDefault();
        let s1={...this.state};
        this.putdata(`http://localhost:2410/student/${s1.detail.id}`,s1.detail)
    }
    render(){
        let{id,name,section,email,password}=this.state.detail;

        return <div className="container my-4">
            <div className="row">
                <div className="col-3"></div>
                <div className="col-4 border bg-light">
                <div className="form-group">
                        <label>Id</label>
                        <input type="text" name="id" readOnly placeholder="Enter Id" value={id}  className="form-control" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Enter Name" value={name} className="form-control" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input type="text" name="email" readOnly placeholder="Enter Email" value={email} className="form-control" onChange={this.handleChange}/>
                      
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="text" name="password"  placeholder="Enter Password" value={password} className="form-control" onChange={this.handleChange}/>
                       
                    </div>
                    <div className="text-center">
                        <button className="btn btn-primary btn-sm" onClick={this.handleSubmit}>Submit</button>
                    </div>
                </div>
            </div>
        </div>
    }
}