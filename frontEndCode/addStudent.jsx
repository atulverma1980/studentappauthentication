import axios from "axios";
import React from "react";
import http from "./../../services/httpService";
export default class AddStudent extends React.Component{
    state={
        detail:{id:"",name:"",section:"",maths:"",english:"",computers:"",email:"",gender:"",password:"",role:""},
        edit:false,
        sec:["A","B","C","D"],
        error:{},
    }
    async componentDidMount(){
        let {id}=this.props.match.params;
        if(id){
            try{
            let user = localStorage.getItem("user");
       // let response = await http.get(`/student/${id}`);
       let response = await axios.get(`http://localhost:2410/student/${id}`,{
           headers:{Authorization:user}
       })
        let {data}=response;
        let s1={...this.state};
        let arr = data.map(v=>{
        s1.detail.id=v.id;
        s1.detail.name=v.name;
        s1.detail.section=v.section;
        s1.detail.maths=v.maths;
        s1.detail.english=v.english;
        s1.detail.computers=  v.computers;
        s1.detail.email=v.email;
        s1.detail.password=v.password;
        s1.detail.gender=v.gender;
        s1.detail.role=v.role;
        });
        s1.edit=true;
        this.setState(s1);
    }catch(ex){
        if(ex.response && ex.response.status==401){
            localStorage.removeItem("role") 
            localStorage.removeItem("user")
            window.location="/login"
        }
    }
        }
    }
    handleChange=e=>{
        const{currentTarget:input}=e
        let s1={...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
       }
   async postdata(url,obj){
       try{
        let response = await axios.post(url,obj);
        this.props.history.push("/student");
       }catch(ex){
        if(ex.response && ex.response.status==401){
            window.location="/login"
        }
       }
    }
    async putdata(url,obj){
        try{
        let response = await axios.put(url,obj);
        this.props.history.push("/student");
        }catch(ex){
            if(ex.response && ex.response.status==401){
                window.location="/login" 
            }
        }
    }
    handleSubmit=e=>{
        e.preventDefault();
        let s1={...this.state};
        let errors=this.validateForm(s1.detail);
        if(this.isValid(errors)){
            s1.edit?this.putdata(`http://localhost:2410/student/${s1.detail.id}`,s1.detail):
            this.postdata("http://localhost:2410/student",s1.detail);
        }else{
            s1.error = errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return count==0;
    }
    validateForm=(arr)=>{
        let{id,name,section,maths,english,computers,email,password,role}=arr;
        let errors={};
        errors.id = this.validateId(id);
        errors.name= this.validateName(name);
        errors.section = this.validateSection(section);
        errors.maths = this.validateMarks(maths);
        errors.english = this.validateMarks(english);
        errors.computers = this.validateMarks(computers);
        errors.email = this.validateEmail(email);
        errors.password = this.validatePass(password);
        errors.role = this.validateRole(role);
        return errors;
    }
    checkId=(val)=>{
        const arr=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(let i=0;i<arr.length;i++){
            if(val[0]==arr[i]){
                return true
            }
        }
    }
    checkId2=(val)=>{
        const arr=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(let i=0;i<arr.length;i++){
            if(val[1]==arr[i]){
                return true
            }
        }
    }
    checkId1=(val)=>{
       let str = val.substring(2);
       if(isNaN(str)){
           return true;
       }
    }
    validateId=(val)=>
    !val?"Please Enter Id":val.length<6?"Id must be 6 digit":!this.checkId(val)?"First two digit must be Alphabets in capital letter.":!this.checkId2(val)?"First two digit must be Alphabets in capital letter.":this.checkId1(val)?"Last 4 must be Digit":""
    validateName=(val)=>
    !val?"Please Enter Name":"";
    validateSection=(val)=>
    !val?"Please Select Section":""
    validateMarks=(val)=>
    !val?"Please Enter Marks":"";
    validateEmail=(val)=>
    !val?"Please Enter Email":!val.includes("@")?"Please enter valid email":"";
    validatePass=(val)=>
    !val?"Please Enter Password":"";
    validateRole=(val)=>
    !val?"Please Enter Role Admin/Student":"";
    render(){
        let{id,name,section,maths,english,computers,email,gender,password,role}=this.state.detail;
        const{error}=this.state;

        return<div className="container my-4">
            <div className="row">
                <div className="col-2"></div>
                <div className="col-4 bg-light border">
                    <h4 className="text-center text-danger border bg-info">Class Detail</h4><hr/>
                    <div className="form-group">
                        <label>Id</label>
                        <input type="text" name="id" placeholder="Enter Id" value={id} readOnly={this.state.edit} className="form-control" onChange={this.handleChange}/>
                        {error.id?<span className="text-danger">{error.id}</span>:""}
                    </div>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Enter Name" value={name} className="form-control" onChange={this.handleChange}/>
                        {error.name?<span className="text-danger">{error.name}</span>:""}
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Gender:- </label>
                        <div className="col-5">
                            <div className="form-check-inline">
                                <input type="radio" name="gender" className="form-check-input" value="Male"  checked={gender=="Male"} onChange={this.handleChange}/>
                                <label className="form-check-label">Male</label>
                            </div>
                        </div>
                        <div className="col-5">
                            <div className="form-check-inline">
                                <input type="radio" name="gender" className="form-check-input" value="Female"  checked={gender=="Female"} onChange={this.handleChange}/>
                                <label className="form-check-label">Female</label>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Section</label>
                    <select className="form-control" name="section" value={section} onChange={this.handleChange}>
                        <option value="">Select Section</option>
                        {this.state.sec.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.section?<span className="text-danger">{error.section}</span>:""}
                    </div>
                    <hr/>
                    <h4 className="text-center text-danger border bg-info">Detail of Marks</h4>
                    <hr/>
                    <div className="form-group">
                        <label>Math</label>
                        <input type="text" name="maths" placeholder="Marks of Maths" value={maths} className="form-control" onChange={this.handleChange}/>
                        {error.maths?<span className="text-danger">{error.maths}</span>:""}
                    </div>
                    <div className="form-group">
                        <label>English</label>
                        <input type="text" name="english" placeholder="Marks of English" value={english} className="form-control" onChange={this.handleChange}/>
                        {error.english?<span className="text-danger">{error.english}</span>:""}
                    </div>
                    <div className="form-group">
                        <label>Computer</label>
                        <input type="text" name="computers" placeholder="Marks of Computer" value={computers} className="form-control" onChange={this.handleChange}/>
                        {error.computers?<span className="text-danger">{error.computers}</span>:""}
                    </div>
                  
               </div>
                    <div className="col-1"></div>
                    <div className="col-4 border bg-light">
                    <h4 className="text-center text-danger border bg-info">Login Detail</h4><hr/>
                    <div className="form-group">
                        <label>Email</label>
                        <input type="text" name="email" placeholder="Enter Email"  readOnly={this.state.edit} value={email} className="form-control" onChange={this.handleChange}/>
                        {error.email?<span className="text-danger">{error.email}</span>:""}
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="text" name="password" readOnly={this.state.edit} placeholder="Enter Password" value={password} className="form-control" onChange={this.handleChange}/>
                        {error.password?<span className="text-danger">{error.password}</span>:""}
                    </div>
                  
                    <div className="form-group">
                        <label>Role</label>
                        <input type="text" name="role" placeholder="Enter role"  readOnly={this.state.edit} value={role} className="form-control" onChange={this.handleChange}/>
                        {error.role?<span className="text-danger">{error.role}</span>:""}
                    </div>
                    <div className="text-center">
                        <button className="btn btn-primary btn-sm" onClick={this.handleSubmit}>{this.state.edit?"Update":"Submit"}</button>
                    </div>
                    </div>
            </div>
        </div>
    }
}