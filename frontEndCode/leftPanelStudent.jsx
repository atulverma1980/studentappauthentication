import React from "react";
export default class LeftPanel extends React.Component{
    state={
       sec:["A", "B", "C", "D"],
       marks:["Excellent", "Average", "Good", "Poor"],
       
    }
   
    makeCB=(arr,value,name,label)=>(
        <React.Fragment>
            <label className="font-weight-bold">{label}</label>
            {arr.map(v=><div className="form-check"key={v}>
                <input type="checkbox" className="form-check-input" name={name} value={v}
                checked={value.findIndex(a=>a===v)>=0} onChange={this.handleChange}/>
                <label className="form-check-label">{v}</label>
            </div>)}
        </React.Fragment>
    );
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let opt = {...this.props.option};
        opt[input.name]=this.updateCB(opt[input.name],input.checked,input.value);
        this.props.onOptionChange(opt);
    }
    updateCB=(inpVal,checked,value)=>{
        let inpArr = inpVal?inpVal.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index = inpArr.findIndex(v=>v===value);
            if(index>=0){
                inpArr.splice(index,1);
            }
        }
        return inpArr.join(",");
    }
    // handleChange=(e)=>{
    //     const{currentTarget:input}=e;
    //     let s1={...this.props.option};
    //     input.type==="checkbox"?s1[input.name]=this.updateCB(s1[input.name],input.checked,input.value):s1[input.name]=input.value;
    //     this.props.onOptionChange(s1);
    // }
    render(){
        const{sec,marks}=this.state;
        const{section="",maths="",english="",computers=""}=this.props.option;
        return<div className="row">
            <div className="col-12">
            {this.makeCB(sec,section.split(","),"section","Choose Section")}<hr/>
            {this.makeCB(marks,maths.split(","),"maths","Maths")}<hr/>
            {this.makeCB(marks,english.split(","),"english","English")}<hr/>
            {this.makeCB(marks,computers.split(","),"computers","Computers")}

            </div>
        </div>
    }
}