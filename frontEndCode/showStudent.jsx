import React from "react";
import http from "../../services/httpService";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import {Link} from "react-router-dom";
import queryString from "query-string";
import LeftPanel from "./leftPanelStudent";
import axios from "axios";
export default class ShowStudent extends React.Component{
    state={
        detail:[],
       
    }
    async fetchData(){
        const queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchStr(queryParams);
        const user = localStorage.getItem("user");
       // let response = await http.get(`/student?${searchStr}`);
       try{
       let response = await axios.get(`http://localhost:2410/student?${searchStr}`,{
        headers:{Authorization:user}
       });
        let{data}=response;
        this.setState({detail:data});
    }catch(ex){
        if(ex.response && ex.response.status==401){
            localStorage.removeItem("role")
            localStorage.removeItem("user")
            window.location="/login";
        }
    }
    }
     componentDidMount(){
      this.fetchData()
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props){
            this.fetchData();
        }
    }
    handleChange=(opt)=>{
        opt.page=1;
        this.callURL("/student",opt);
    }
    callURL=(url,option)=>{
        let searchStr = this.makeSearchStr(option);
        this.props.history.push({
            pathname:url,
            search:searchStr
        });
    }
    makeSearchStr=(option)=>{
        const{section,maths,english,computers,sortBy,page}=option;
        let searchStr="";
        searchStr=this.addToQuery(searchStr,"section",section);
        searchStr=this.addToQuery(searchStr,"maths",maths);
        searchStr=this.addToQuery(searchStr,"english",english);
        searchStr=this.addToQuery(searchStr,"sortBy",sortBy);
        searchStr=this.addToQuery(searchStr,"computers",computers);
        searchStr=this.addToQuery(searchStr,"page",page);
        return searchStr;
    }
    sort=(colNo)=>{
        const queryParams = queryString.parse(this.props.location.search);
        switch (colNo) {
        case 0:
             queryParams.sortBy="id";
             this.callURL("/student",queryParams);
             break;
        case 1:  
             queryParams.sortBy="name";
             this.callURL("/student",queryParams);
             break;
        case 2:
             queryParams.sortBy="section";
             this.callURL("/student",queryParams);
             break;
        case 3:
            queryParams.sortBy="maths";
            this.callURL("/student",queryParams); 
            break;
        case 4:
            queryParams.sortBy="english";
            this.callURL("/student",queryParams);   
            break;
        case 5:
            queryParams.sortBy="computers";
            this.callURL("/student",queryParams);
             break;
         default:break;         
        }
    }
    handlePage=(incr)=>{
        const queryParam = queryString.parse(this.props.location.search);
        let {page="1"}=queryParam;
        let newPage = +page+incr;
        queryParam.page=newPage;
        this.callURL("/student",queryParam);
    }
    addToQuery=(str,name,val)=>
    val?str?`${str}&${name}=${val}`:`${name}=${val}`:str;
    render(){
        const{data=[],startIndex,endIndex,numOfItems}=this.state.detail;
        const queryParams = queryString.parse(this.props.location.search);
        return <div className="container my-4 bg-light">
            <div className="row">
                <div className="col-2">
                    <LeftPanel  option={queryParams} onOptionChange={this.handleChange}/>
                </div>
                <div className="col-10">
                    Showing {startIndex} to {endIndex} of {numOfItems}
            <div className="row bg-info text-dark text-center">
                <div className="col-1 border"onClick={()=>this.sort(0)}>Id</div>
                <div className="col-2 border"onClick={()=>this.sort(1)}>Name</div>
                <div className="col-2 border"onClick={()=>this.sort(2)}>Section</div>
                <div className="col-2 border"onClick={()=>this.sort(3)}>Maths</div>
                <div className="col-2 border"onClick={()=>this.sort(4)}>English</div>
                <div className="col-2 border"onClick={()=>this.sort(5)}>Computers</div>
                <div className="col-1 border"></div>
            </div>
            {data.map(v=><div className="row text-center" key={v.id}>
            <div className="col-1 border">{v.id}</div>
                <div className="col-2 border">{v.name}</div>
                <div className="col-2 border">{v.section}</div>
                <div className="col-2 border">{v.maths}</div>
                <div className="col-2 border">{v.english}</div>
                <div className="col-2 border">{v.computers}</div>
                <div className="col-1 border">
               <Link to={`/editDetail/${v.id}`}><FontAwesomeIcon className="m-1" icon={faEdit}/></Link>
               <Link to={`/student/${v.id}`}> <FontAwesomeIcon className="m-1" icon={faTrash}/></Link>
                </div>
            </div>)}
            <div className="row">
                <div className="col-2">
                {startIndex>1?<button className="btn btn-success" onClick={()=>this.handlePage(-1)}>Previous</button>:""}
                </div>
                <div className="col-8"></div>
                <div className="col-2">
                {endIndex==numOfItems?"":<button className="btn btn-success"onClick={()=>this.handlePage(1)}>Next</button>}
                </div>
            </div>

            </div>
            </div>
        </div>
    }
}