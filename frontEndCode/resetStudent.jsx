import axios from "axios";
import React from "react";
import http from "./../../services/httpService";
export default class ResetStudent extends React.Component{
   async componentDidMount(){
    window.location="/student";
       let response = await axios.delete("http://localhost:2410/student/reset");
   }
   
   render(){
       return <h5>Resetting table...</h5>
   }
}