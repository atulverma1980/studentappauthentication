import React from "react";
export default class Logout extends React.Component{

    logout=()=>{
        let user = localStorage.removeItem("user");
        localStorage.removeItem("role");
        window.location="/login";
    }
    render(){
        this.logout();
        return <div></div>
    }
}