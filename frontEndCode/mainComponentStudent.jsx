import React from "react";
import {Route,Redirect,Switch} from "react-router-dom";
import AddStudent from "./addStudent";
import Delete from "./deleteStudent";
import Logout from "./empLogout";
import Navbar from "./navbarStudent";
import ShowStudent from "./showStudent";
import StudentDetail from "./studentDetail";
import Login from "./studentLogin";
import axios from "axios";
import EditStudent from "./editStudent";
import ResetStudent from "./resetStudent";
export default class MainComponent extends React.Component{
 
    render(){
        const user =  localStorage.getItem("role");
      
        return <div className="container-fluid">
            <Navbar/>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/logout" component={Logout}/>
                <Route path="/editStudent"
                render={(props)=>user==="Student"?<EditStudent {...props}/>:<Redirect to="/login"/>}/>
                <Route path="/student/:id" 
                 render={(props)=>user==="Admin"?<Delete {...props}/>:<Redirect to="/login"/>}/>
                <Route path="/student"
                 render={(props)=>user==="Admin"?<ShowStudent {...props}/>:<Redirect to="/login"/>}/> 
                <Route path="/addstudent"
                 render={(props)=>user==="Admin"?<AddStudent {...props}/>:<Redirect to="/login"/>}/>
                 <Route path="/reset" component={ResetStudent}/>
                 <Route path="/detail" 
                 render={(props)=>user==="Student"?<StudentDetail {...props}/>:<Redirect to="/login"/>}/>
                <Route path="/editDetail/:id"
                 render={(props)=>user==="Admin"?<AddStudent {...props}/>:<Redirect to="/login"/>}/>
                <Redirect from="" to="/login"/>
            </Switch>
        </div>
    }
}