import axios from "axios";
import React from "react";
export default class StudentDetail extends React.Component{
    state={
        data:[]
    }
    async componentDidMount(){
        const user  = localStorage.getItem("user");
        try{
        let response = await axios.get("http://localhost:2410/student/detail",{
            headers:{Authorization:user}
        });
        let {data}=response;
        this.setState({data:data});
    }catch(ex){
        if(ex.response && ex.response.status==401){
            localStorage.removeItem("user");
            localStorage.removeItem("role")
            window.location="/login"
        }
    }
    }
    edit=()=>{
        this.props.history.push("/editStudent");
    }
    render(){
        const {data=[]}=this.state;
        return <div className="container my-4">
            <div className="row">
                <div className="col-4"></div>
                <div className="col-5 text-center border bg-light">
                    <b>ID  :- </b>{data.map(v=>v.id)}<br/>
                    <b>Name :- </b>{data.map(v=>v.name)}<br/>
                    <b>Gender :- </b>{data.map(v=>v.gender)}<br/>
                    <b>Email :- </b><u>{data.map(v=>v.email)}</u><br/>
                    <h3>Marks</h3>
                    <b>Maths :- </b>{data.map(v=>v.maths)}<br/>
                    <b>English :- </b>{data.map(v=>v.english)}<br/>
                    <b>Computers :- </b>{data.map(v=>v.computers)}<br/>
        <button className="btn btn-secondary btn-sm" onClick={()=>this.edit()}>Edit</button>
                </div>
            </div>
        </div>
    }
}