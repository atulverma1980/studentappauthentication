var mysql = require('mysql');
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var passport = require("passport");
var passportjwt = require("passport-jwt");
var config = require("./src/node/N3/config.js"); 
var ExtractJwt = passportjwt.ExtractJwt;
var Strategy=passportjwt.Strategy;
var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers,Origin,Accept,Authorization,X-Requested-With,Content-Type,Access-Control-Request");
  res.header("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
  res.header("Access-Control-Allow-Credentials",true);
  res.header("Access-Control-Expose-Headers","*");
  next();
  });
  var params={
    secretOrKey:config.jwtSecret,
    jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken()
  };
  app.use(bodyParser.json());
  passport.initialize();
  app.listen(2410,()=>console.log("Listening to the Port 2410"));
  const jwtExpirySeconds=300
//creating connection 
var con = mysql.createConnection({
  host :"localhost",
  user: "root",
  password :"root",
  database:"mydb",
});
//connecting to the database
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query("create database if not exists mydb",function(err,result){
      if(err) throw err;
      console.log("Database Created");
});
  var sql = "create table if not exists students (id varchar(10),name varchar(15),section varchar(10),maths int(10),english int(10),computers int(10),gender varchar(20),email varchar(40),password varchar(20),role varchar(15),primary key(email))";
  con.query(sql,function(err,result){
      if(!err){
        insertData();
        console.log("table created");
       } else{
        throw err;
       }
    });
 
});
//Inserting values to the created table
 insertData=()=>{
      var sql = "insert ignore into students values?";
      var values=[
        ["AB1234","Jack","Admin",0,0,0,"Male","jack@gmail.com","jack123","Admin"],
        ["AB4321","Mary","B",76,67,84,"Female","mary@gmail.com","mary123","Student"],
        ["BA1234","Edward","D",75,55,82,"Male","edward@gmail.com","edward123","Student"],
        ["BA4321","Juli","C",67,76,87,"Female","juli@gmail.com","juli123","Student"],
        ["QW3456","Sumit","A",69,52,83,"Male","sumit@gmail.com","sumit123","Student"],
        ["QA1245","Mohit","B",65,58,81,"Male","mohit@gmail.com","mohit123","Admin"],
        ["CF4689","Rohan","C",62,65,66,"Male","rohan@gmail.com","rohan123","Student"],
        ["MK8811","Govind","A",60,68,65,"Male","govind@gmail.com","govind123","Student"],
        ["LK6590","Neha","B",61,70,55,"Female","neha@gmail.com","neha123","Student"],
        ["UY6578","Mark","C",78,72,59,"Male","mark@gmail.com","mark123","Student"],
        ["XF7821","Jhon Dav","C",80,75,62,"Male","jhon@gmail.com","jhon123","Student"],
        ["GH2134","Vikram","D",85,71,61,"Male","vikram@gmail.com","vikram123","Student"],
        ["MK4221","Nimit","A",82,76,60,"Male","nimit@gmail.com","nimit123","Student"],
        ["XC1122","Aparna","Admin",0,0,0,"Female","aprna@gmail.com","aparna123","Admin"],
        ["WE2332","Shyam","B",87,74,77,"Male","shyam@gmail.com","shyam123","Student"],
        ["RX2378","Daniel","B",89,85,73,"Male","daniel@gmail.com","daniel123","Student"],
        ["FG4333","Cristi","A",81,91,90,"Female","cristi@gmail.com","cristi123","Student"],
        ["DS3344","Romeo","C",86,96,92,"Male","romeo@gmail.com","romeo123","Student"],
        ["MH4222","Monika","C",85,83,95,"Female","monika@gmail.com","monika123","Student"],
        ["UU7456","Elon","A",83,87,82,"Male","elon@gmail.com","elon123","Student"],
        ["DR8356","Cris Moris","B",81,80,87,"Male","cris@gmail.com","cris123","Student"],
        ["VB6578","Rekha","C",55,88,89,"Female","rekha@gmail.com","rekha123","Student"],
        ["HG4678","Rohit","C",54,78,93,"Male","rohit@gmail.com","rohit123","Student"],
        ["FG8746","Sohan","Admin",0,0,0,"Male","sohan@gmail.com","sohan123","Admin"],
        ["TG6743","Gaurav","A",58,63,68,"Male","gaurav@gmail.com","gaurav123","Student"],
        ["HY7781","Sanjeev","D",90,93,96,"Male","sanjeev@gmail.com","sanjeev123","Student"],
        ["OK7467","Anubhav","C",91,75,77,"Male","anubhav@gmail.com","anubhav123","Student"],
        ["GH3466","Lokesh","B",94,77,72,"Male","lokesh@gmail.com","lokesh123","Student"],
        ["ZA4366","Ritika","Admin",0,0,0,"Female","ritika@gmail.com","ritik123","Admin"],
        ["ZA4321","Rachit","B",97,53,83,"Male","rachit@gmail.com","rachit123","Student"],
      ]
      con.query(sql,[values],function(err,result){
          if(err) throw err;
          console.log("rows affected "+result.affectedRows);
      });
     
 }
 var strategy1 = new Strategy(params,function(payload,done){
  // console.log("I m calling");
  let sqlQuery = 'select * from students';
  con.query(sqlQuery,(err,result,field)=>{
    if(err) throw err;
    let index = result.findIndex(v=>v.id===payload.id);
    var user=result[index]||null;
    if(user){
      return done(null,{
          id:user.id
      })
  }else{
      return done(new Error("User not Found"),null);
  }
  });
});
var strategy2 = new Strategy(params,(payload,done)=>{
    let sqlQuery = 'select * from students';
    con.query(sqlQuery,(err,result,field)=>{
      if(err) throw err;
      let index = result.findIndex(v=>v.id===payload.id);
      var user=result[index]||null;
      if(user){
          if(user.role=="Admin"){
        return done(null,{
            id:user.id
        })
        }else{
            return done(new Error("You are not logged as Admin"),null)
        }
    }else{
        return done(new Error("User not Found"),null);
    }
    });
})
passport.use("local.one",strategy1);
passport.use("local.two",strategy2);

app.post("/login",(req,res)=>{
  if(req.body.email && req.body.password){
    var email=req.body.email;
    var password=req.body.password;
    con.query("select * from students",(err,result,field)=>{
      if(err) throw err;
      var user = result.find(v=>v.email===email && v.password===password);
      if(user){
        var payload={
          id:user.id
        };
        var token=jwt.sign(payload,config.jwtSecret,{
          algorithm:"HS256",
          expiresIn:jwtExpirySeconds
        });
      
        res.setHeader("X-Auth-Token",token);
        res.json({success:true,token:"bearer "+token})
      }else{
        res.sendStatus(401);
      }
    })
  }else{
    res.sendStatus(401);
  }
});
//get logged employee
app.get("/student/detail",passport.authenticate("local.one",{session:false}),(req,res)=>{
  let sqlQuery = `select * from students where id="${req.user.id}"`;
  con.query(sqlQuery,(err,result,field)=>{
    if(err) throw err;
    res.send(result);
  });
})
//get all employee
 app.get("/student",passport.authenticate("local.two",{session:false}),(req,res)=>{
  let sqlQuery = 'select * from students where 1 = 1';
  let page = req.query.page ? +req.query.page : 1;
  params=[];
  let section= req.query.section?req.query.section.split(","):undefined;
  let sortBy=req.query.sortBy;
  let english = req.query.english?req.query.english.split(","):undefined;
  let computers = req.query.computers?req.query.computers.split(","):undefined;
  let maths = req.query.maths?req.query.maths.split(","):undefined;
  if(section!==undefined){
    sqlQuery+=` and section in (${section.map(v=>`"${v}"`)})`;
  }
 
    con.query(sqlQuery,(err,result,fields)=>{
      if(err) throw err;
    
  if(sortBy!==undefined){
  let arr;
  switch(sortBy){
    case "id":arr=result.sort((s1,s2)=>s1.id.localeCompare(s2.id));break;
    case "name":arr=result.sort((s1,s2)=>s1.name.localeCompare(s2.name));break;
    case "section":arr = result.sort((s1,s2)=>s1.section.localeCompare(s2.section));break;
    case "maths":arr=result.sort((s1,s2)=>(s1.maths-s2.maths));break;
    case "english":arr=result.sort((s1,s2)=>(s1.english-s2.english));break;
    case "computers":arr=result.sort((s1,s2)=>(s1.computers-s2.computers));break;
  default:break;
}
      result=arr;
  }
  if(maths!=undefined){
   let arr1 = result.filter(v=>
      maths.find(a=>a=="Excellent"?v.maths>80:a=="Average"?
      v.maths>=60 && v.maths<80:a=="Good"?
      v.maths>=40 && v.maths<60:v.maths<40));
      result = arr1;
  }
  if(english!=undefined){
    let arr1 = result.filter(v=>
      english.find(a=>a=="Excellent"?v.english>80:a=="Average"?
      v.english>=60 && v.english<80:a=="Good"?
      v.english>=40 && v.english<60:v.english<40));
      result = arr1;
  }
  if(computers!=undefined){
    let arr1 = result.filter(v=>
      computers.find(a=>a=="Excellent"?v.computers>80:a=="Average"?
      v.computers>=60 && v.computers<80:a=="Good"?
      v.computers>=40 && v.computers<60:v.computers<40));
      result = arr1;
  }
       res.send(makeData(page,6,result));
    });
});
//make data paginated
let makeData = (pageNum, size, data1) => {
    let startIndex = (pageNum - 1) * size;
    let endIndex =
      data1.length > startIndex + size - 1
        ? startIndex + size - 1
        : data1.length - 1;
    let data2 = data1.filter(
      (lt, index) => index >= startIndex && index <= endIndex
    );
    let dataFull = {
      startIndex: data1.length > 0 ? startIndex + 1 : startIndex,
      endIndex: data1.length > 0 ? endIndex + 1 : endIndex,
      numOfItems: data1.length,
      data: data2,
    };
    return dataFull;
  };
  //post employee
  app.post("/student",(req,res)=>{
    let body = req.body;
    let sqlQuery = 'insert into students values ?';
    let values=[
      [body.id,body.name,body.section,body.maths,body.english,body.computers,body.gender,body.email,body.password,body.role],
    ];
    con.query(sqlQuery,[values],(err,result)=>{
      if(err){
        res.status(401).send('Email already exists');
        throw err;
      }
      console.log("row affected :"+result.affectedRows);
      res.send(body);
    })
  });
  //update employee
  app.put("/student/:id",(req,res)=>{
    let id = req.params.id;
    let body = req.body;
    let sqlQuery = `update students set name="${body.name}",section="${body.section}",maths=${body.maths},english=${body.english},computers=${body.computers},gender="${body.gender}",email="${body.email}",password="${body.password}",role="${body.role}" where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err) throw err
      console.log("row Updated :"+result.affectedRows+" ID :-",id);

      res.send(body);
    })
  });
  //delete employee
  app.delete("/student/delete/:id",(req,res)=>{
    let id = req.params.id;
    let sqlQuery = `delete from students where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err)throw err
      console.log("Record Deleted having id :-"+id);
    })
  });
  //get particular seleted employee
  app.get("/student/:id",passport.authenticate("local.one",{session:false}),(req,res)=>{
    let id = req.params.id;
    let sqlQuery = `select * from students where id="${id}"`;
    con.query(sqlQuery,(err,result)=>{
      if(err) throw err;
      res.send(result);
    })
  });
  //Resetting table
  app.delete("/student/reset",(req,res)=>{
    let sqlQuery = 'delete from students';
    con.query(sqlQuery,(err,result)=>{
      if(err)throw err;
      console.log("Table is Resetting");
      insertData();
    });
  
  });
